# Intersect SDK Core
**Intersect SDK Core** contains common functionality shared between all other Intersect SDK packages

## Changelog
See `CHANGELOG.md` for all current and released features/changes

## Source
https://bitbucket.org/hidalgorides/intersect-sdk-core

## Installation via Composer
Include the following code snippet inside your project `composer.json` file (update if necessary)
```
"repositories": [
  {
    "type": "vcs",
    "url": "https://bitbucket.org/hidalgorides/intersect-sdk-core"
  }
],
"require" : {
  "intersect/sdk-core" : "^1.0.0"
}
```