<?php

namespace Intersect\SDK\Core;

class HttpRequestData {

    private $body;
    private $formData;
    private $headers;
    private $jsonData;
    private $multipartData;

    public function getBody()
    {
        return $this->body;
    }

    public function setBody($body)
    {
        $this->body = $body;
    }

    public function getFormData()
    {
        return $this->formData;
    }

    public function setFormData(array $formData)
    {
        $this->formData = $formData;
    }

    public function getHeaders()
    {
        return $this->headers;
    }

    public function setHeaders(array $headers)
    {
        $this->headers = $headers;
    }

    public function getJsonData()
    {
        return $this->jsonData;
    }

    public function setJsonData(array $jsonData)
    {
        $this->jsonData = $jsonData;
    }

    public function getMultipartData()
    {
        return $this->multipartData;
    }

    public function setMultipartData(array $multipartData)
    {
        $this->multipartData = $multipartData;
    }

}