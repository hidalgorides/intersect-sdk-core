<?php

namespace Intersect\SDK\Core;

use GuzzleHttp\Client;
use Intersect\SDK\OAuthClient;

abstract class HttpClient implements OAuthClient {

    private $accessToken;
    private $clientId;
    private $clientSecret;
    private $redirectUri;
    protected $scopes = [];

    /** @var Client */
    private $client;

    abstract protected function getBaseAuthorizationEndpoint();

    public function __construct($options)
    {
        $this->client = new Client();

        if (!is_array($options))
        {
            $this->setAccessToken($options);
        }
        else
        {
            $this->clientId = $this->getOptionValue($options, 'client-id');
            $this->clientSecret = $this->getOptionValue($options, 'client-secret');
            $this->redirectUri = $this->getOptionValue($options, 'redirect-uri');

            $scopes = $this->getOptionValue($options, 'scopes');
            if (!is_null($scopes))
            {
                if (!is_array($scopes))
                {
                    $scopes = [$scopes];
                }
                $this->scopes = $scopes;
            }
        }
    }

    public function getAccessToken()
    {
        return $this->accessToken;
    }

    public function setAccessToken($accessToken) 
    {
        $this->accessToken = $accessToken;
    }

    /**
     * @param $state
     * @param $responseType
     * @return string
     */
    public function generateAuthorizationUrl($state, $responseType = 'code')
    {
        $queryParameters = [
            'client_id' => $this->getClientId(),
            'state' => $state,
            'response_type' => $responseType,
            'redirect_uri' => $this->getRedirectUri()
        ];

        $scopes = $this->getScopes();
        if (count($scopes) > 0) 
        {
            $queryParameters['scope'] = implode(',', $scopes);
        }

        return $this->getBaseAuthorizationEndpoint() . '?' . http_build_query($queryParameters);
    }

    /**
     * @param $method
     * @param $uri
     * @param array $data
     * @param array $headers
     * @return HttpClientResponse
     * @throws HttpClientException
     */
    public function request($method, $uri, HttpRequestData $httpRequestData = null)
    {
        $options = [];

        if (!is_null($httpRequestData))
        {
            if (!is_null($httpRequestData->getJsonData()))
            {
                $options['json'] = $httpRequestData->getJsonData();
            }
            else if (!is_null($httpRequestData->getFormData()))
            {
                $options['form_params'] = $httpRequestData->getFormData();
            }
            else if (!is_null($httpRequestData->getMultipartData()))
            {
                $options['multipart'] = [
                    $httpRequestData->getMultipartData()
                ];
            }
            else if (!is_null($httpRequestData->getBody()))
            {
                $options['body'] = $httpRequestData->getBody();
            }

            if (!is_null($httpRequestData->getHeaders()))
            {
                $options['headers'] = $httpRequestData->getHeaders();
            }
        }

        try {
            $response = $this->client->request($method, $uri, $options);

            return new HttpClientResponse($response->getBody(), $response->getStatusCode());
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            throw new HttpClientException($e);
        }
    }

    protected function getClientId()
    {
        return $this->clientId;
    }

    protected function getClientSecret()
    {
        return $this->clientSecret;
    }

    protected function getRedirectUri()
    {
        return $this->redirectUri;
    }

    protected function getScopes()
    {
        return $this->scopes;
    }

    /**
     * @param array $options
     * @param $key
     * @return mixed|null
     */
    protected function getOptionValue(array $options, $key)
    {
        return (array_key_exists($key, $options) ? $options[$key] : null);
    }

}