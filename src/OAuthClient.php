<?php

namespace Intersect\SDK\Core;

interface OAuthClient {

    public function generateAuthorizationUrl($state, $responseType = 'code');

    public function getAccessTokenDetails($code);

    public function refreshAccessToken($refreshToken);

    public function setAccessToken($accessToken);

}