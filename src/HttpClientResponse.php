<?php

namespace Intersect\SDK\Core;

class HttpClientResponse {

    private $body;
    private $statusCode;

    public function __construct($body, $statusCode)
    {
        $this->body = $body;
        $this->statusCode = $statusCode;
    }

    public function getBody()
    {
        return $this->body;
    }

    public function getStatusCode()
    {
        return $this->statusCode;
    }

}